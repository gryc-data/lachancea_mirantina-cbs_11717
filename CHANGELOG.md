# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

### Fixed

* LTR in wrong strand: LAMI_0A07569T
* Useless feature (annotation note): LAMI_0D13410T
* LTR in wrong strand: LAMI_0B03554T
* LTR in wrong strand: LAMI_0E09032T
* LTR in wrong strand: LAMI_0C02960T
* LTR in wrong strand: LAMI_0C08328T
* LTR in wrong strand: LAMI_0C10330T
* LTR in wrong strand: LAMI_0F15368T
* LTR in wrong strand: LAMI_0F15632T
* LTR in wrong strand: LAMI_0H06260T
* LTR in wrong strand: LAMI_0H11276T
* LTR in wrong strand: LAMI_0H17942T
* LTR in wrong strand: LAMI_0G14928T
* LTR in wrong strand: LAMI_0G15786T
* LTR in wrong strand: LAMI_0G15852T
* Mobile_element feature with multiple exon: LAMI_0G15786T

## v1.0 (2021-05-17)

### Added

* The 8 annotated chromosomes of Lachancea mirantina CBS 11717 (source EBI, [GCA_900074745.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074745.1)).
