## *Lachancea mirantina* CBS 11717

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12932](https://www.ebi.ac.uk/ena/browser/view/PRJEB12932)
* **Assembly accession**: [GCA_900074745.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074745.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LAMI0
* **Assembly length**: 10,117,267
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,414,338 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5110
* **Pseudogene count**: 54
* **tRNA count**: 205
* **rRNA count**: 9
* **Mobile element count**: 33
